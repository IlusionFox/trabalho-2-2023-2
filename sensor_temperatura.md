# Leitura do Sensor de Temperatura

Neste trabalho estão disponibilizados os seguintes sensores nas seguintes placas:

| Placa | Sensor | Modo de Leitura |
|:--|:-:|:-:|
| **rasp40** | BME280 | I2C |  
| **rasp43** | BMP280 | I2C |  
| **rasp44** | BMP280| Módulo de Kernel |  
| **rasp46** | BMP280 | I2C |  
| **rasp47** | BMP280 | I2C |  

## Sensor BME280

### Requisitos para python:
- `pip install smbus2`
- `pip install RPi.bme280`

Exemplo: [https://randomnerdtutorials.com/raspberry-pi-bme280-python/](https://randomnerdtutorials.com/raspberry-pi-bme280-python/)

### Requisitos em C:

Driver da Bosh: [https://github.com/boschsensortec/BME280_driver](https://github.com/boschsensortec/BME280_driver)

Utilizar o código de exemplo `linux_userspace.c` na pasta `examples/contributed`.

## Sensor BMP280

### Requisitos para python:
- `pip install smbus2`
- `pip install bmp280`

Exemplo: [https://iotstarters.com/configuring-bmp280-sensor-with-raspberry-pi/](https://iotstarters.com/configuring-bmp280-sensor-with-raspberry-pi/)  
Lib em Python: [https://github.com/pimoroni/bmp280-python](https://github.com/pimoroni/bmp280-python)

## Sensor BMP280 - Módulo do Kernel

A alternativa ao acesso direto ao I2C é habilitar o driver do kernel para o sensor BMP280. Ao habilitar, o kernel fica responsável por amostrar os dados e disponibilizar em arquivos na pasta `/sys/bus/iio/devices/iio:device0`. Neste caso, basta ler os arquivos com os dados desejados:

- `in_pressure_input` para a pressão. É necessário mutiplicar por 10 para obter o valor correto;
- `in_temp_input` para a temperatura. É necessário calcular o valor da temperatura utilizando a seguinte equação: `temperatura = (((in_temp_input / 1000.0) * 100) + 0.5) / 100`;

Exemplo: [https://forums.raspberrypi.com/viewtopic.php?t=329871](https://forums.raspberrypi.com/viewtopic.php?t=329871)
